import numpy as np
import pandas as pd
import os
import shutil


def main():
    # Check if an argument is provided
    print("HI FROM THE CONTAINER")
    df = pd.read_csv('/app/dummy_inp.csv')
    df["Y"] = np.sin(df["X"])
    df.to_csv("out.csv", index=False)

    create_artifact()



def create_artifact():
    if not os.path.exists("output"):
        print(os.getcwd())
        # Create the directory and any necessary parent directories
        os.makedirs("output")

    print(f"copying file from /app/out.csv to {os.getcwd()}/output/out.csv")
    shutil.copy("/app/out.csv", "output/out.csv")

    

if __name__ == "__main__":
    main()