import time



def main():
    # Define the duration of the loop in seconds
    duration = 10 * 60  # 10 minutes

    # Get the starting time
    start_time = time.time()
    count = 1
    # Perform the loop until the duration is reached
    while time.time() - start_time < duration:
        # Your loop code here

        # Sleep for a short interval (optional)
        time.sleep(10)  # Sleep for 1 second
        print(f"step {count}")
        count += 1

if __name__ == "__main__":
    main()